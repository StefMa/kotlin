package guru.stefma.kotlin.test

fun justAFunctionWithoutClass() {

    println("Wow!!")

}

fun functionWithReturnValue(): Int {

    return 5

}

fun testParams(param1: String, param2: Int = -1) {

    if (param2 == -1) {
        println("Please declare param2!!")
        println("No worry, I'll do it for you with param2 = 2 ;)")
        testParams(param1, param2 = 2)
        return
    }

    println("Param1: $param1")
    println("Param2: $param2")

}