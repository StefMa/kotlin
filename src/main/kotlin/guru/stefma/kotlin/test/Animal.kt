package guru.stefma.kotlin.test

/**
 * Created by stefan on 19.07.17.
 */
class Animal {

    val mArms: Int

    var mName: String = ""

    var mAge: Int = 0

    constructor(arms: Int) {
        mArms = arms
    }


}