package guru.stefma.kotlin.test

class MyFirstKotlinClass {

    fun sayHello() {

        println("Hello")

    }

}

class ClassWithConstructorAndCustomGetter(val name: String) {

    val anotherName: String
        get() {
            if (name == "Name") {
                return name
            }
            return "$name not match \"Name\". So we will return another value for anotherName"
        }

}

data class Laptop(val graka: String = "", var ramInGb: Int = 0, val screenSizeInInch: Double = 0.0)