package guru.stefma.kotlin

import java.util.*

interface CatStrokeListener {

    fun onTailStroked()

    fun onHeadStroked() {
        println("Miau! That is goood :loveheart:")
    }

    fun onAnotherPlaceStroked()

    val strokeByHand: Boolean
        get() {
            return true
        }

}

class Cat : CatStrokeListener {

    override fun onTailStroked() {
        println("Seriously? :/")
    }

    override fun onAnotherPlaceStroked() {
        println("WOOOW. Don't touch me!")
    }

    override fun onHeadStroked() {
        super.onHeadStroked()
        println("Can you do it again, pls?")
    }

}

class CatOwner {

    private val cat = Cat()

    fun strokeCat(): Boolean {
        strokeRandomPlace(cat)
        return cat.strokeByHand
    }

    fun strokeAnotherCat(listener: CatStrokeListener): Boolean {
        strokeRandomPlace(listener)
        return listener.strokeByHand
    }

    fun strokeThirdCat(): Boolean {
        val listener: CatStrokeListener = object : CatStrokeListener {

            override val strokeByHand: Boolean
                get() = false

            override fun onTailStroked() {
                println("WTH? You don't know what I like - don't implement this listener by yourself -.-")
            }

            override fun onAnotherPlaceStroked() {
                println("WTH? You don't know what I like - don't implement this listener by yourself -.-")
            }

            override fun onHeadStroked() {
                println("WTH? You don't know what I like - don't implement this listener by yourself -.-")
            }

        }
        strokeRandomPlace(listener)
        return listener.strokeByHand
    }

    private fun strokeRandomPlace(listener: CatStrokeListener) {
        val int = Random().nextInt(3)
        when (int) {
            0 -> listener.onHeadStroked()
            1 -> listener.onTailStroked()
            else -> listener.onAnotherPlaceStroked()
        }
    }

}