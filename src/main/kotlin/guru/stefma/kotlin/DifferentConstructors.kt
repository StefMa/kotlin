package guru.stefma.kotlin

// All classes/fields etc are final in Kotlin. Needs to be declared as open to override it
open class PrimaryConst(aString: String) {

    init {
        println("This string ->$aString<- is only visible in the init{} method")
    }

}

class OverridePrimaryConst(val aString: String) : PrimaryConst("CoolOverrideString") {

    fun aMethod() {
        printSomething("This string ->$aString<- is visible in the class")
    }

}

open class SecondaryConst {

    constructor() : this(2) {
        println("This constructor will call the other constructor with the default value 2")
    }

    constructor(aInteger: Int) {
        println("Secondary constructor - not recommended with kotlin. Anyway. The value is ->$aInteger")
    }

}

class OverrideSecondaryConst : SecondaryConst {

    constructor() : this("5") {
        println("This will call the other constructor with 5 as a String")
    }

    constructor(aString: String) : super(aString.toInt()) {
        println("Call super with $aString and convert to int.")
    }

}

