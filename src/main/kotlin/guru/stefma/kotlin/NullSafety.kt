package guru.stefma.kotlin

fun ignoreTheMayBeNull(aInt: Int?) {

    val newInt = aInt!!.inc()
    println(newInt)

}

fun printOnlyIfNotNull(aInt: Int?) {

    // Will only be called if aInt is not null
    val newInt = aInt?.inc()
    // Inside the let block we can make sure that int is not null!
    newInt?.let { println(newInt) }

}

fun simpleElvisOperator(aInt: Int?) {

    val fallbackInt = -1
    val newInt = if (aInt == null) fallbackInt else aInt
    println(newInt)

}

fun elvisOperator(aInt: Int?) {

    // This is something like "If aInt is not null, call inc() and return. Else -1"
    val newInt = aInt?.inc() ?: -1
    println(newInt)

}

fun nicerElvisOperator(aInt: Int?) {

    val fallbackInt = -1
    val newInt = aInt ?: fallbackInt
    println(newInt)

}

fun smartCastPrintOnlyIfNotNull(any: Any?) {

    // We don't know if we can cast any to String. So try it with ?
    val aStringOrNull = any as? String
    // As above. We make sure that "it" is not null
    aStringOrNull?.let { println(it) }

}
