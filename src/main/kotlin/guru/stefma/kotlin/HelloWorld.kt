package guru.stefma.kotlin

import guru.stefma.kotlin.test.*
import java.util.*

val Animal.eyes: Int
    get() = 5

fun Animal.fullNameWithAge(name: String, age: Int) {
    mName = name
    mAge = age
}

fun Animal.fullNameWithAge(): String {
    return "Name: $mName; Age: $mAge"
}

fun main(args: Array<String>) {

    println("Hello Word!")

    printSomething("Something")

    justAFunctionWithoutClass()

    println(functionWithReturnValue())

    val firstClass = MyFirstKotlinClass()
    firstClass.sayHello()

    testParams("FirstParam")

    testParams("FirstParam", 9)

    val any = aRandomReturnValue()
    println("Try to determine which type that is: $any")
    when (any) {
        is String -> printSomething("It's a String!")
        is Int -> printSomething("It's a Integer!")
        else -> printSomething("Sry, I really don't know what type it is :(")
    }

    println(expressionReturn())

    var constructor = ClassWithConstructorAndCustomGetter("Name")
    println(constructor.name)
    println(constructor.anotherName)
    constructor = ClassWithConstructorAndCustomGetter("AnotherName")
    println(constructor.name)
    println(constructor.anotherName)

    val laptop = Laptop()
    println(laptop.toString())

    val macBook = Laptop("Intel Graphics 640", ramInGb = 16, screenSizeInInch = 15.2)
    println(macBook.toString())
    macBook.ramInGb = 32
    println("Upgraded my MacBook ram to ${macBook.ramInGb}!")
    println(macBook.toString())

    when {
        macBook.screenSizeInInch >= 15.2 -> println("Wow, such a big Laptop? :o")
        else -> println("What a small laptop. Sweet :)")
    }

    val animal = Animal(2)
    println("Print animal arms ${animal.mArms}")
    println("Print eyes from extension property: ${animal.eyes}")
    println(animal.fullNameWithAge())
    animal.fullNameWithAge("Name", 5)
    println(animal.fullNameWithAge())

    PrimaryConst("AString")
    println("-----")
    OverridePrimaryConst("AnotherString (").aMethod()
    println("-----")
    SecondaryConst()
    println("-----")
    SecondaryConst(4)
    println("-----")
    OverrideSecondaryConst()
    println("-----")
    OverrideSecondaryConst("99")

    ThisIsASingleton.showMoneyCoins()
    println("\n-----")
    ThisIsASingleton.addNewMoneyCoin(9.0)
    ThisIsASingleton.showMoneyCoins()
    println("\n-----")
    ThisIsASingleton.removeMoneyCoinIfAvailable(3434.0)
    ThisIsASingleton.showMoneyCoins()
    println("\n-----")
    ThisIsASingleton.removeMoneyCoinIfAvailable(9.0)
    ThisIsASingleton.showMoneyCoins()
    println("\n-----")

    val falseValue = ClassWithCompanion.falseValue()
    falseValue.whatsTheValue()
    val trueValue = ClassWithCompanion.trueValue()
    trueValue.whatsTheValue()

    val instance = ClassWithExtendedCompanion.instance() as Any
    println("Instance of extended companion is extended companion: ${instance is ClassWithExtendedCompanion}")
    val anotherInstance = ClassWithExtendedCompanion.anotherInstance() as Any
    println("AnotherInstance of extended companion is extended companion: ${anotherInstance is ClassWithExtendedCompanion}")
    println("It is instance of ClassWithCompanion: ${anotherInstance is ClassWithCompanion}")

    val catOwner = CatOwner()
    val strokeByHand = catOwner.strokeCat()
    println("Was that stroke by hand? $strokeByHand")
    val strokeByHandThird = catOwner.strokeThirdCat()
    println("Was that stroke by hand? $strokeByHandThird")
    val strokeByHandOwn = catOwner.strokeAnotherCat(object : CatStrokeListener {
        override fun onTailStroked() {
            println("How do you know? That is awesome!")
        }

        override fun onAnotherPlaceStroked() {
            println("I like it... depends on... Ok, not really!")
        }

    })
    println("Was that stroke by hand? $strokeByHandOwn")

    lambdaFun()
    lambdaFunWithArgs()

    ignoreTheMayBeNull(2)
    try {
        ignoreTheMayBeNull(null)
    } catch (e: NullPointerException) {
        println("We have thrown a NPE :/")
    }
    println("-----")
    printOnlyIfNotNull(3)
    printOnlyIfNotNull(null)
    println("-----")
    simpleElvisOperator(99)
    simpleElvisOperator(null)
    println("-----")
    elvisOperator(12)
    elvisOperator(null)
    println("-----")
    nicerElvisOperator(92)
    nicerElvisOperator(null)
    println("-----")
    smartCastPrintOnlyIfNotNull(3)
    smartCastPrintOnlyIfNotNull("hi")

}

fun printSomething(something: String) {

    println(something)

}

fun aRandomReturnValue(): Any {

    val int = Random().nextInt(3)

    when (int) {

        0 -> return 5
        1 -> return "A String"
        2 -> return false
        else -> return 99f

    }

}

fun expressionReturn(): String = "Expression"