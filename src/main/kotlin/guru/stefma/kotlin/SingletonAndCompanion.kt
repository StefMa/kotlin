package guru.stefma.kotlin

object ThisIsASingleton {

    val availableMoneyCoins = arrayListOf(0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1, 2)

    fun showMoneyCoins() {
        for ((index, money) in availableMoneyCoins.withIndex()) {
            print(money)
            if (index != availableMoneyCoins.size - 1) {
                print(",")
            }
        }
    }

    fun addNewMoneyCoin(coin: Double) {
        availableMoneyCoins.add(coin)
        println("New coin added: $coin")
    }

    fun removeMoneyCoinIfAvailable(coin: Double) {
        if (availableMoneyCoins.contains(coin)) {
            val remove = availableMoneyCoins.remove(coin)
            if (remove) {
                println("Removed coin $coin")
            } else {
                println("Can't remove coin $coin. Seems it is not available")
            }
            return
        }
        println("Can't remove coin $coin. Seems it is not available")
    }

}

class ClassWithCompanion private constructor(private val aVar: Boolean) {

    companion object {

        fun trueValue() = ClassWithCompanion(true)

        fun falseValue(): ClassWithCompanion {
            val companion = ClassWithCompanion(false)
            companion.aStringChangedbyCompanion = "ChangedDefaultString"
            return companion
        }

    }

    private var aStringChangedbyCompanion = "DefaultString"

    fun whatsTheValue() {
        println("The current value is ->$aVar<-")
        println("The string value is set to ->$aStringChangedbyCompanion<-")
    }

}

class ClassWithExtendedCompanion private constructor() {

    companion object ExpendMePls {

        fun instance(): ClassWithExtendedCompanion {
            return ClassWithExtendedCompanion()
        }

    }

}

fun ClassWithExtendedCompanion.ExpendMePls.anotherInstance(): ClassWithCompanion {
    return ClassWithCompanion.falseValue()
}