package guru.stefma.kotlin

fun lambdaFun() {

    val lambda = { println("Lambda as var") }
    setClickListener(lambda)

    setClickListener({ println("Lambda as \"direct\" parameter ") })

    setClickListener { println("Lambda as last parameter. Without () ") }

}

fun lambdaFunWithArgs() {


    val lambda = { println("Lambda as var") }
    setClickListener(2, lambda)

    setClickListener(3, { println("Lambda as \"direct\" parameter ") })

    setClickListener(2) { println("Lambda as last parameter. Without () ") }

}

// Takes a lambda as argument and run it. Lambda should return [Unit]
// Similar to run() ;)
fun setClickListener(function: () -> Unit) {

    function.invoke()

}

fun setClickListener(times: Int, function: () -> Unit) {

    for (i in 0 until times) {
        function.invoke()
    }

}
